package github

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/Jeffail/tunny"
	"github.com/manveru/scylla/git"
	macaron "gopkg.in/macaron.v1"
)

type config interface {
	GetGithubUser() string
	GetGithubToken() string
}

func PostHook(pool *tunny.Pool, config config) func(ctx *macaron.Context, hook Hook) {
	return func(ctx *macaron.Context, hook Hook) {
		if ctx.Req.Header.Get("X-Github-Event") == "pull_request" {
			if hook.Action != "closed" {
				go process(config, pool, &hook, progressHost(ctx))
			}
		}

		ctx.JSON(200, map[string]string{"status": "OK"})
	}
}

func process(config config, pool *tunny.Pool, hook *Hook, host string) {
	j := &Job{Hook: hook, Host: host, config: config}
	j.Status("pending", "Queueing...")
	_, err := pool.ProcessTimed(j, time.Minute*30)
	if err == tunny.ErrJobTimedOut {
		j.Status("error", "Timeout after 30 minutes")
		log.Printf("Build of %s %s timed out\n", j.CloneURL(), j.SHA())
	}
}

type Job struct {
	config config
	Hook   *Hook
	Host   string
}

func NewJobFromJSONFile(path string) *Job {
	file, err := os.Open(path)
	if err != nil {
		log.Printf("Couldn't open file %s: %s\n", path, err)
		return nil
	}

	job := &Job{Hook: &Hook{}}
	if err = json.NewDecoder(file).Decode(job.Hook); err != nil {
		log.Printf("Failed to decode JSON %s: %s\n", path, err)
		return nil
	}
	return job
}

func (j *Job) Build() string {
	git.Clone(j)

	j.persistHook()

	git.Build(j)

	return "OK"
}

func (j *Job) FullName() string {
	return j.Hook.Repository.FullName
}

func (j *Job) targetURL() string {
	uri, _ := url.Parse(j.Host)
	uri.Path = fmt.Sprintf("/builds/%s/%s", git.SaneFullName(j), j.SHA())
	return uri.String()
}

func (j *Job) CloneURL() string {
	return j.Hook.PullRequest.Head.Repo.CloneURL
}

func (j *Job) SHA() string {
	return j.Hook.PullRequest.Head.Sha
}

func (j *Job) persistHook() {
	pathName := filepath.Join(git.BuildDir(j), "github_hook.json")
	file, err := os.Create(pathName)
	if err != nil {
		log.Printf("Failed to create file %s: %s\n", pathName, err)
		return
	}
	defer file.Close()
	err = json.NewEncoder(file).Encode(j.Hook)
	if err != nil {
		log.Printf("Failed to write file %s: %s\n", pathName, err)
	}
}

// Mapping
//
// Us       | GitLab   | GitHub
// ---------+----------+---------
// success  |   -.-    |   -.-
// pending  |   -.-    |   -.-
// failure  | failed   |   -.-
// running  |   -.-    | pending
// failed   |   -.-    | failure
// canceled |   -.-    | error
// error    | canceled |   -.-

func (j *Job) Status(state, description string) {
	if len(description) > 138 {
		description = description[0:138]
	}

	switch state {
	case "failed":
		state = "failure"
	case "canceled":
		state = "error"
	case "running":
		state = "pending"
	}

	status := map[string]string{
		"state":       state,
		"target_url":  j.targetURL(),
		"description": description,
		"context":     "Scylla",
	}
	body := &bytes.Buffer{}

	json.NewEncoder(body).Encode(&status)

	req, err := http.NewRequest("POST", j.Hook.PullRequest.StatusesURL, body)
	if err != nil {
		log.Fatalf("Failed creating request: %s", err)
	}

	req.SetBasicAuth(j.config.GetGithubUser(), j.config.GetGithubToken())

	_, err = http.DefaultClient.Do(req)
	if err != nil {
		log.Printf("Error while calling Github API: %s", err)
	}
}

func cleanJoin(parts ...string) string {
	return filepath.Clean(filepath.Join(parts...))
}

func progressHost(ctx *macaron.Context) string {
	proto := ctx.Req.Header.Get("X-Forwarded-Proto")
	if proto == "" {
		proto = "http"
	}
	return fmt.Sprintf("%s://%s", proto, ctx.Req.Host)
}
