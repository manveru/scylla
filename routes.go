package main

import (
	"github.com/go-macaron/binding"
	"github.com/manveru/scylla/github"
	"github.com/manveru/scylla/gitlab"
	macaron "gopkg.in/macaron.v1"
)

func setupRouting(m *macaron.Macaron) {
	m.Get("/", getIndex)
	m.Get("/builds", getBuilds)
	m.Get("/builds/:project", getBuildsProject)
	m.Get("/builds/:project/:id", getBuildsProjectId)
	m.Get("/projects", getProjects)
	m.Post("/github/hook", binding.Bind(github.Hook{}), github.PostHook(pool, config))
	m.Post("/gitlab/hook", binding.Bind(gitlab.Hook{}), gitlab.PostHook(pool, config))
}
