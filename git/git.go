package git

import (
	"bytes"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"

	"github.com/manveru/scylla/util"
)

type Repo interface {
	CloneURL() string
	SHA() string
	FullName() string
	Status(string, string)
}

func Clone(r Repo) {
	_ = os.RemoveAll(SourceDir(r))

	r.Status("running", "Cloning...")

	util.RunCmd(exec.Command(
		"git", "clone", r.CloneURL(), SourceDir(r)))

	r.Status("running", "Checkout...")

	util.RunCmd(exec.Command(
		"git",
		"-c", "advice.detachedHead=false",
		"-C", SourceDir(r),
		"checkout", r.SHA()))
}

func Build(r Repo) error {
	log.Printf("Starting work on %s %s...", r.CloneURL(), r.SHA())

	r.Status("running", "Nix Build...")
	stdout, stderr, err := Nix(
		r,
		"build", "--out-link", ResultLink(r), "-f", CiNixPath(r))

	writeOutput(r, stdout, stderr)

	if err != nil {
		r.Status("failure", err.Error())
		return errors.New("Nix Failure: " + err.Error())
	}

	r.Status("success", "Evaluation succeeded")

	_ = os.RemoveAll(SourceDir(r))

	return nil
}

func writeOutput(r Repo, stdout, stderr *bytes.Buffer) {
	writeOutputToFile(r, "stdout", stdout)
	writeOutputToFile(r, "stderr", stderr)
}

func writeOutputToFile(r Repo, baseName string, output *bytes.Buffer) {
	pathName := filepath.Join(BuildDir(r), baseName)
	file, err := os.Create(pathName)
	if err != nil {
		log.Printf("Failed to create file %s: %s\n", pathName, err)
		return
	}
	defer file.Close()
	_, err = output.WriteTo(file)
	if err != nil {
		log.Printf("Failed to write file %s: %s\n", pathName, err)
	}
}

func RootDir() string {
	return "./ci"
}

func ResultLink(r Repo) string {
	return filepath.Join(BuildDir(r), "result")
}

func CiNixPath(r Repo) string {
	return filepath.Join(BuildDir(r), "source", "ci.nix")
}

var sanitizeURLPath = regexp.MustCompile(`[^a-zA-Z0-9-]+`)

func SaneFullName(r Repo) string {
	return sanitizeURLPath.ReplaceAllString(r.FullName(), "_")
}

func BuildDir(r Repo) string {
	return cleanJoin(RootDir(), SaneFullName(r), r.SHA())
}

func cleanJoin(parts ...string) string {
	return filepath.Clean(filepath.Join(parts...))
}

func SourceDir(r Repo) string {
	return filepath.Join(BuildDir(r), "source")
}

func Pname(r Repo) string {
	return SaneFullName(r) + "-" + r.SHA()
}

func Nix(r Repo, subcmd string, args ...string) (*bytes.Buffer, *bytes.Buffer, error) {
	return util.RunCmd(exec.Command(
		"nix",
		append([]string{
			subcmd,
			"--allow-import-from-derivation",
			"--auto-optimise-store",
			"--enforce-determinism",
			"--fallback",
			"--http2",
			"--keep-build-log",
			"--restrict-eval",
			"--show-trace",
			"--max-build-log-size", "10000000",
			"--max-silent-time", "30",
			"--timeout", "30",
			"--option", "allowed-uris", "https://github.com/ https://source.xing.com/",
			"-I", "./nix",
			"-I", SourceDir(r),
			"--argstr", "pname", Pname(r),
		}, args...)...,
	))
}

func NixLog(r Repo) (string, string, error) {
	sout, serr, err := Nix(r, "log", "-f", CiNixPath(r), "")
	if err == nil {
		return sout.String(), serr.String(), err
	}

	stderrPath := filepath.Join(BuildDir(r), "stderr")
	stderrBytes, err := ioutil.ReadFile(stderrPath)
	if err != nil {
		return "", "", errors.New("No trace of logs found")
	}

	drvs := parseDrvsFromStderr(stderrBytes)
	for _, drv := range drvs {
		sout, serr, err = util.RunCmd(exec.Command("nix", "log", drv))
	}

	return sout.String(), serr.String(), err
}

var matchFailine = regexp.MustCompile(`error: build of .+ failed`)
var matchFailDrvs = regexp.MustCompile(`[^'\s]+\.drv`)

func parseDrvsFromStderr(input []byte) []string {
	line := matchFailine.FindString(string(input))
	return matchFailDrvs.FindAllString(line, -1)
}
