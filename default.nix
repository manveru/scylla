{ stdenv
, lib
, buildGoPackage
, callPackage
, fetchFromGitHub
, go-bindata
, git
, golangci-lint
, lint ? true }:
let
  bindata = import ./nix/bindata.nix;
  cleanSourceFilter = name: type: let baseName = baseNameOf (toString name); in ! (
    lib.hasSuffix "~" baseName ||
    builtins.match "^\\.sw[a-z]$" baseName != null ||
    builtins.match "^\\..*\\.sw[a-z]$" baseName != null ||
    (type == "symlink" && lib.hasPrefix "result" baseName)
  );
in buildGoPackage rec {
  name = "scylla-unstable-${version}";
  version = "2018-07-29";
  goPackagePath = "github.com/manveru/scylla";
  src = lib.cleanSourceWith { filter = cleanSourceFilter; src = ./.; };
  goDeps = ./deps.nix;
  nativeBuildInputs = [ go-bindata ]
    ++ lib.optional lint [ git golangci-lint ];

  preBuild = ''
    # go generate ${goPackagePath}
    cd go/src/${goPackagePath}
    cp ${bindata { pkgName = "public"; dir = ./public; }} public/bindata.go
    cp ${bindata { pkgName = "templates"; dir = ./templates; }} templates/bindata.go
  '' + lib.optionalString lint ''
    mkdir -p $bin
    golangci-lint run --out-format json --new > $bin/lint.json
  '';

  meta = {
    description = "A simple, easy to deploy Nix Continous Integration server";
    homepage = https://github.com/manveru/scylla;
    license = stdenv.lib.licenses.mit;
    maintainers = stdenv.lib.maintainers.manveru;
    platforms = stdenv.lib.platforms.unix;
  };
}
