{ pkgs ? import ./nix/nixpkgs.nix }: {
  meta = {
    name = "scylla";
    maintainer = "Michael Fellinger <mf@seitenschmied.at>";
  };
  scylla = pkgs.callPackage ./. {};
  docker = pkgs.callPackage ./nix/docker.nix {};
  lint = pkgs.callPackage ./. { lint = true; };
  deep = pkgs.recurseIntoAttrs {
    scylla = pkgs.callPackage ./. {};
    thisIsNotEvaluated = {
      scylla = pkgs.callPackage ./. {};
    };
  };
}
