# file generated from Gopkg.lock using dep2nix (https://github.com/nixcloud/dep2nix)
[
  {
    goPackagePath  = "github.com/BurntSushi/toml";
    fetch = {
      type = "git";
      url = "https://github.com/BurntSushi/toml";
      rev =  "a368813c5e648fee92e5f6c30e3944ff9d5e8895";
      sha256 = "1sjxs2lwc8jpln80s4rlzp7nprbcljhy5mz4rf9995gq93wqnym5";
    };
  }
  {
    goPackagePath  = "github.com/Jeffail/tunny";
    fetch = {
      type = "git";
      url = "https://github.com/Jeffail/tunny";
      rev =  "59cfa8fcb19f5acba8db07a0dd3cb2fa7edbc228";
      sha256 = "1g3aikbzhzq2i3iihwr8c15l44mma1r1srks5c23fb04wlqkrh2w";
    };
  }
  {
    goPackagePath  = "github.com/Unknwon/bra";
    fetch = {
      type = "git";
      url = "https://github.com/Unknwon/bra";
      rev =  "c14c7f7994e36691f39c2443aa6f7be6d111456f";
      sha256 = "1fq1qk1i14q80s2v8ijf2rnn8qia57frfm94flhj6k02i12gr0ny";
    };
  }
  {
    goPackagePath  = "github.com/Unknwon/com";
    fetch = {
      type = "git";
      url = "https://github.com/Unknwon/com";
      rev =  "da59b551951d50441ca26b7a8cd81317f34df87f";
      sha256 = "03xx2kwi8pmglblvfxasppyvpbscq8l1r03r6jfkgi8c2a8i01am";
    };
  }
  {
    goPackagePath  = "github.com/Unknwon/log";
    fetch = {
      type = "git";
      url = "https://github.com/Unknwon/log";
      rev =  "e617c87089d3ff2ae05098eac8c9386069cd8461";
      sha256 = "08w0hwkzrg59g8i72i147i8rsgs9g6v1w2kd5vmq1ykyhh1vpzx8";
    };
  }
  {
    goPackagePath  = "github.com/alexflint/go-arg";
    fetch = {
      type = "git";
      url = "https://github.com/alexflint/go-arg";
      rev =  "f7c0423bd11ee80ab81d25c6d46f492998af8547";
      sha256 = "0b0pl8fv86lzgx7jhcwz5zk4263l55x38lvq1sf3fwj0jn0ji636";
    };
  }
  {
    goPackagePath  = "github.com/alexflint/go-scalar";
    fetch = {
      type = "git";
      url = "https://github.com/alexflint/go-scalar";
      rev =  "e80c3b7ed292b052c7083b6fd7154a8422c33f65";
      sha256 = "1mwn4mdc8fqcchvvxj4gmm4zy40jv0l34w1556wxkf0z2w610rfb";
    };
  }
  {
    goPackagePath  = "github.com/elazarl/go-bindata-assetfs";
    fetch = {
      type = "git";
      url = "https://github.com/elazarl/go-bindata-assetfs";
      rev =  "38087fe4dafb822e541b3f7955075cc1c30bd294";
      sha256 = "0wxpvzw3503xfp756qvyq9qgckmilwkzglxxfd0azqnkw58jn1hc";
    };
  }
  {
    goPackagePath  = "github.com/go-macaron/bindata";
    fetch = {
      type = "git";
      url = "https://github.com/go-macaron/bindata";
      rev =  "85786f57eee3e5544a9cc24fa2afe425b97a8652";
      sha256 = "0wj3paj8956jzcmsn44hsday9pnl46xp43nmyv5730980w72kj1n";
    };
  }
  {
    goPackagePath  = "github.com/go-macaron/binding";
    fetch = {
      type = "git";
      url = "https://github.com/go-macaron/binding";
      rev =  "ac54ee249c27dca7e76fad851a4a04b73bd1b183";
      sha256 = "07cxgd78g5978z7d380rwji9y5ypi9764b69cs4crncgvdyylvzj";
    };
  }
  {
    goPackagePath  = "github.com/go-macaron/inject";
    fetch = {
      type = "git";
      url = "https://github.com/go-macaron/inject";
      rev =  "d8a0b8677191f4380287cfebd08e462217bac7ad";
      sha256 = "0p47pz699xhmi8yxhahvrpai9r49rqap5ckwmz1dlkrnh3zwhrhh";
    };
  }
  {
    goPackagePath  = "github.com/gopherjs/gopherjs";
    fetch = {
      type = "git";
      url = "https://github.com/gopherjs/gopherjs";
      rev =  "0892b62f0d9fb5857760c3cfca837207185117ee";
      sha256 = "06q1x4l0y6rwxmpgpqlj07njw04hlg8n3s479sbv7zj3jqhgxww1";
    };
  }
  {
    goPackagePath  = "github.com/jtolds/gls";
    fetch = {
      type = "git";
      url = "https://github.com/jtolds/gls";
      rev =  "77f18212c9c7edc9bd6a33d383a7b545ce62f064";
      sha256 = "1vm37pvn0k4r6d3m620swwgama63laz8hhj3pyisdhxwam4m2g1h";
    };
  }
  {
    goPackagePath  = "github.com/smartystreets/assertions";
    fetch = {
      type = "git";
      url = "https://github.com/smartystreets/assertions";
      rev =  "e900ae048470ba3ce0440bc5ba1baf2bd87a9a1d";
      sha256 = "1awlx291mlhar1k8d5alryvcfhmd9099gxz9rl23vk9svcqa1gbn";
    };
  }
  {
    goPackagePath  = "github.com/smartystreets/goconvey";
    fetch = {
      type = "git";
      url = "https://github.com/smartystreets/goconvey";
      rev =  "ef6db91d284a0e7badaa1f0c404c30aa7dee3aed";
      sha256 = "16znlpsms8z2qc3airawyhzvrzcp70p9bx375i19bg489hgchxb7";
    };
  }
  {
    goPackagePath  = "github.com/urfave/cli";
    fetch = {
      type = "git";
      url = "https://github.com/urfave/cli";
      rev =  "8e01ec4cd3e2d84ab2fe90d8210528ffbb06d8ff";
      sha256 = "0cpr10n4ps3gcdbcink71ry9hzhdb5rrcysmylybs8h2lzxqgc1i";
    };
  }
  {
    goPackagePath  = "golang.org/x/crypto";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/crypto";
      rev =  "c126467f60eb25f8f27e5a981f32a87e3965053f";
      sha256 = "0xvvzwxqi1dbrnsvq00klx4bnjalf90haf1slnxzrdmbadyp992q";
    };
  }
  {
    goPackagePath  = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev =  "bd9dbc187b6e1dacfdd2722a87e83093c2d7bd6e";
      sha256 = "0zj8s3q2fznmap1nfr8pv4hz8xqixmkyhr6slq4baf8rvcb4mvbj";
    };
  }
  {
    goPackagePath  = "gopkg.in/fsnotify/fsnotify.v1";
    fetch = {
      type = "git";
      url = "https://github.com/fsnotify/fsnotify";
      rev =  "c2828203cd70a50dcccfb2761f8b1f8ceef9a8e9";
      sha256 = "07va9crci0ijlivbb7q57d2rz9h27zgn2fsm60spjsqpdbvyrx4g";
    };
  }
  {
    goPackagePath  = "gopkg.in/ini.v1";
    fetch = {
      type = "git";
      url = "https://github.com/go-ini/ini";
      rev =  "d58d458bec3cb5adec4b7ddb41131855eac0b33f";
      sha256 = "1c7zca10gjgjmlxzv2q8bkc2aphzfflngvdrg1zlbvqcalkvfa2z";
    };
  }
  {
    goPackagePath  = "gopkg.in/macaron.v1";
    fetch = {
      type = "git";
      url = "https://github.com/go-macaron/macaron";
      rev =  "88a29ec402caf29b5f22f7817d884981bc6b48d9";
      sha256 = "1lxshbvw82x5zbcr93y0vahj8b4fawjiac24kp7q62wwqdmz718p";
    };
  }
]