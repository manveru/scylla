package main

import (
	"html/template"
	"io/ioutil"
	"runtime"
	"strings"
	"time"

	macaron "gopkg.in/macaron.v1"

	"github.com/Jeffail/tunny"
	arg "github.com/alexflint/go-arg"
	"github.com/go-macaron/bindata"
	"github.com/manveru/scylla/github"
	"github.com/manveru/scylla/gitlab"
	"github.com/manveru/scylla/public"
	"github.com/manveru/scylla/templates"
)

//go:generate make public/bindata.go templates/bindata.go

type Config struct {
	GithubUser  string `arg:"--github-user,env:GITHUB_USER"`
	GithubToken string `arg:"--github-token,env:GITHUB_TOKEN"`
	GitlabToken string `arg:"--gitlab-token,env:GITLAB_TOKEN"`
	Host        string `arg:"env:HOST"`
	Port        int    `arg:"env:PORT"`
}

func (c Config) GetGithubUser() string {
	return c.GithubUser
}

func (c Config) GetGithubToken() string {
	return c.GithubToken
}

func (c Config) GetGitlabToken() string {
	return c.GitlabToken
}

var (
	config = &Config{}
	pool   *tunny.Pool
)

func main() {
	parseConfig()

	pool = tunny.NewFunc(runtime.NumCPU(), worker)

	defer pool.Close()

	m := macaron.New()
	m.Use(macaron.Logger())
	m.SetAutoHead(true)

	m.Use(macaron.Static("public",
		macaron.StaticOptions{
			FileSystem: bindata.Static(bindata.Options{
				Asset:      public.Asset,
				AssetDir:   public.AssetDir,
				AssetNames: public.AssetNames,
				Prefix:     "",
			}),
		},
	))

	m.Use(macaron.Renderer(macaron.RenderOptions{
		Layout:     "layout",
		Extensions: []string{".html"},
		Funcs: []template.FuncMap{
			{"FormatTime": func(t time.Time) string {
				return t.Format(time.RFC1123)
			}},
		},
		TemplateFileSystem: bindata.Templates(bindata.Options{
			Asset:      templates.Asset,
			AssetDir:   templates.AssetDir,
			AssetNames: templates.AssetNames,
			Prefix:     "",
		}),
	}))

	setupRouting(m)
	m.Run(config.Host, config.Port)

	// mux := http.NewServeMux()
	// mux.Handle("/", m)

	// graceful.Run(config.Host+":"+config.Port, 2*time.Second, mux)
}

func parseConfig() {
	config.Host = "0.0.0.0"
	config.Port = 8080

	arg.MustParse(config)

	if strings.HasPrefix(config.GithubUser, "/") {
		if content, err := ioutil.ReadFile(config.GithubUser); err != nil {
			config.GithubUser = string(content)
		}
	}

	if strings.HasPrefix(config.GithubToken, "/") {
		if content, err := ioutil.ReadFile(config.GithubToken); err != nil {
			config.GithubToken = string(content)
		}
	}
}

func worker(work interface{}) interface{} {
	switch w := work.(type) {
	case *github.Job:
		return w.Build()
	case *gitlab.Job:
		return w.Build()
	}

	return "Couldn't find work type"
}
