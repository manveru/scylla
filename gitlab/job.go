package gitlab

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"path/filepath"

	"github.com/manveru/scylla/git"
)

type Job struct {
	Hook   *Hook
	Host   string
	config config
}

func (j *Job) Build() string {
	git.Clone(j)

	j.persistHook()

	git.Build(j)

	return "OK"
}

func (j *Job) persistHook() {
	pathName := filepath.Join(git.BuildDir(j), "gitlab_hook.json")
	file, err := os.Create(pathName)
	if err != nil {
		log.Printf("Failed to create file %s: %s\n", pathName, err)
		return
	}
	defer file.Close()
	err = json.NewEncoder(file).Encode(j.Hook)
	if err != nil {
		log.Printf("Failed to write file %s: %s\n", pathName, err)
	}
}

func (j *Job) FullName() string {
	return j.Hook.Project.PathWithNamespace
}

func (j *Job) SHA() string {
	return j.Hook.ObjectAttributes.LastCommit.ID
}

func (j *Job) targetURL() string {
	uri, _ := url.Parse(j.Host)
	uri.Path = fmt.Sprintf("/builds/%s/%s", git.SaneFullName(j), j.SHA())
	return uri.String()
}

func (j *Job) CloneURL() string {
	return j.Hook.ObjectAttributes.Source.URL
}

// In theory this should work, but in practice this doesn't show up anywhere,
// making the whole hook a bit useless.

func (j *Job) Status(state, description string) {
	if len(description) > 138 {
		description = description[0:138]
	}

	switch state {
	case "failure":
		state = "failed"
	case "error":
		state = "canceled"
	}

	postURL, _ := url.Parse(
		fmt.Sprintf("https://gitlab.com/api/v4/projects/%d/statuses/%s",
			j.Hook.Project.ID,
			j.Hook.ObjectAttributes.LastCommit.ID,
		))
	postURL.RawQuery = url.Values{
		"token":       {j.config.GetGitlabToken()},
		"state":       {state},
		"ref":         {j.Hook.ObjectAttributes.Source.DefaultBranch},
		"name":        {"Scylla"},
		"target_url":  {j.targetURL()},
		"description": {description},
		// "coverage": 45.5,
	}.Encode()

	_, err := http.Post(postURL.String(), "", nil)
	if err != nil {
		log.Printf("Error while calling Gitlab API: %s", err)
	}
}

func NewJobFromJSONFile(path string) *Job {
	file, err := os.Open(path)
	if err != nil {
		log.Printf("Couldn't open file %s: %s\n", path, err)
		return nil
	}

	job := &Job{Hook: &Hook{}}
	if err = json.NewDecoder(file).Decode(job.Hook); err != nil {
		log.Printf("Failed to decode JSON %s: %s\n", path, err)
		return nil
	}
	return job
}
