package gitlab

import (
	"fmt"
	"log"
	"time"

	"github.com/Jeffail/tunny"
	macaron "gopkg.in/macaron.v1"
)

type config interface {
	GetGitlabToken() string
}

func PostHook(pool *tunny.Pool, config config) func(ctx *macaron.Context, hook Hook) {
	return func(ctx *macaron.Context, hook Hook) {
		if ctx.Req.Header.Get("X-Gitlab-Event") == "Merge Request Hook" {
			go process(config, pool, &hook, progressHost(ctx))
		}

		ctx.JSON(200, map[string]string{"status": "OK"})
	}
}

func process(config config, pool *tunny.Pool, hook *Hook, host string) {
	j := &Job{Hook: hook, Host: host, config: config}
	j.Status("pending", "Queueing...")
	_, err := pool.ProcessTimed(j, time.Minute*30)
	if err == tunny.ErrJobTimedOut {
		j.Status("error", "Timeout after 30 minutes")
		log.Printf("Build of %s %s timed out\n", j.CloneURL(), j.SHA())
	}
}

func progressHost(ctx *macaron.Context) string {
	proto := ctx.Req.Header.Get("X-Forwarded-Proto")
	if proto == "" {
		proto = "http"
	}
	return fmt.Sprintf("%s://%s", proto, ctx.Req.Host)
}
