self: super:
let
  manveru-nur-packages = fetchTarball {
    url = "https://github.com/manveru/nur-packages/archive/master.tar.gz";
  };
  manveru-nixpkgs = fetchTarball {
    url = https://github.com/manveru/nixpkgs/archive/f8578ebccb4b54506aad71bb38dd9b2578e5af90.tar.gz;
    sha256 = "0m8ashaw7rys1dsgxm9lmic1lcb58cnzk5dcpymkgd9x92x21ax6";
  };
in {
  git-info = (self.callPackage "${manveru-nur-packages}/default.nix" {}).lib.git-info;
  go = super.go_1_10;
  gotools = (import manveru-nixpkgs {}).gotools;
  golangci-lint = (import manveru-nixpkgs {}).golangci-lint;
}
