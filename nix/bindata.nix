let
  pkgs = import ./nixpkgs.nix;
  inherit (pkgs.lib) optionalString;
in
  {dir, pkgName, dev ? false }:

  pkgs.runCommand "${pkgName}-bindata.go" {
    nativeBuildInputs = [pkgs.go-bindata];
    src = dir;
  } (''
    find $src -type d | xargs \
    go-bindata \
      -o $out \
      -prefix ${dir} \
      -pkg ${pkgName} \
      -ignore '\.go' \
      ${optionalString dev "-dev"}
  '' + optionalString dev ''
    echo 'const rootDir = "${pkgName}"' >> $out
  '')
