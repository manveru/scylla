.PHONY: public/bindata.go
public/bindata.go:
	nix build -f nix/bindata.nix --arg dir ./public --argstr pkgName public --arg dev true
	cp result public/bindata.go
	rm result

.PHONY: templates/bindata.go
templates/bindata.go:
	nix build -f nix/bindata.nix --arg dir ./templates --argstr pkgName templates --arg dev true
	cp result templates/bindata.go
	rm result

deps.nix: Gopkg.lock
	dep2nix
